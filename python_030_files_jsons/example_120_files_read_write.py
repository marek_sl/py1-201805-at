text = "This text should be in a nice file"

filename = "my_text.txt"

# a : append -> will append at the end
# w : write -> will overwrite
# r : read

with open(filename, "w") as f:
    f.write(text)


with open(filename, "r") as f:
    content = f.read()
    print content

print "Done"
