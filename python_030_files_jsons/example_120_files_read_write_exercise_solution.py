# write a list with the ingredients salad, orange, mango
# choose randomly an ingredient, and write it to a file called "random_fruit.txt"
import random
food = ["salad", "orange", "mango"]
filename = "random_fruit.txt"
with open(filename, "w") as f:
    f.write(random.choice(food))
