import random

punkte = 0

capitals = {"France": "Paris",
            "Iceland": "Reykjavik",
            "Denmark": "Copenhagen",
            "Lithuania": "Vilnius",
            "Canada": "Ottawa",
            "Austria": "Vienna"}

current_country = random.choice(capitals.keys())

guess = raw_input("What is the capital of: "+current_country+" ?")

if guess.lower() == capitals[current_country].lower():
    punkte += 1
    print "correct"
else:
    print "oh no, the capital of " + current_country + " is " + capitals[current_country]

print "Du hast ", punkte, "Pkte erreicht!"
