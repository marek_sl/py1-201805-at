capitals = {"France": "Paris",
            "Iceland": "Reykjavik",
            "Denmark": "Copenhagen",
            "Lithuania": "Vilnius",
            "Canada": "Ottawa",
            "Austria": "Vienna"}

print capitals["France"]
print capitals.get("France")
print capitals.get("Japan", "Not Found")
