
friends_dict = {}

friends_dict["Monika"] = "Good Friend"
friends_dict["Moritz"] = "Bad Friend"

print friends_dict

del friends_dict["Moritz"]

print friends_dict

friends_dict["Moritz"] = "Bad Friend"

print friends_dict

friends_dict.update({"Moritz": "Good Friend"})

print friends_dict
