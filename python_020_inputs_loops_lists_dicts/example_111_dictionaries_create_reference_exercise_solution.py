# shoppinglist with prices
# "bread", 100
# "butter", 90
# "avocado", 10
# create a dictionary with the food as key, and the price as value
shoppinglist = {"bread": 100, "butter": 90, "avocado": 10}
print shoppinglist

# create a for loop, which iterates through all meals
# print the meal and the price each iteration
for food in ["bread", "butter", "avocado"]:
    print food, shoppinglist[food]


# Extra: sum up all prices and print the total shopping cost
price = sum(shoppinglist.values())
print price


# oder

total = 0
for food, price in shoppinglist.items():
    total+=price
print total