a_dict = {}
points = {"alfred": 10,
          "bettina": 100,
          "christian": 50,
          "doris": 75}

print points  # {'christian': 50, 'bettina': 100, 'doris': 75, 'alfred': 10}

# dict    {key:value, key_2:value_2}
my_dict = {"Monika": 29, "Eva": 39}

#                           0        1      2
# Liste:  essensliste = ["Butter","Brot","Honig"]
#

print my_dict["Monika"]  # 29


