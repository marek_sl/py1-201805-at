class VehicleManager(object):
    def __init__(self):
        self.vehicles = []

    def add_vehicle(self, vehicle):
        self.vehicles.append(vehicle)

    def print_vehicles(self):
        print self.vehicles


class Vehicle(object):
    def __init__(self, brand, model):
        self.brand = brand
        self.model = model

if __name__ == '__main__':
    manager = VehicleManager()
    ferrari = Vehicle("Ferrari", "Spider")
    manager.add_vehicle(ferrari)
    manager.print_vehicles()
