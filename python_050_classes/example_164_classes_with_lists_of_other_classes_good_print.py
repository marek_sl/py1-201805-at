class VehicleManager(object):
    def __init__(self):
        self.vehicles = []

    def add_vehicle(self, vehicle):
        self.vehicles.append(vehicle)

    def print_vehicles(self):
        print "{}".format(self.vehicles)


class Vehicle(object):
    def __init__(self, brand, model):
        self.brand = brand
        self.model = model

    def __str__(self):
        return "Vehicle(brand={}, model={})".format(self.brand, self.model)

    def __repr__(self):
        return "Vehicle(brand={}, model={})".format(self.brand, self.model)


if __name__ == '__main__':
    manager = VehicleManager()
    ferrari = Vehicle("Ferrari", "Spider")
    manager.add_vehicle(ferrari)
    manager.print_vehicles()
