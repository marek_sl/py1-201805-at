class Human(object):
    def __init__(self, age, name):
        self.age = age
        self.name = name


if __name__ == '__main__':
    alfred = Human(19, "alfred")
    print alfred.age  # access attributes with "."
    print alfred.name

    beatrix = Human(22, "beatrix")
    print beatrix.age
    print beatrix.name
