greeting = "Hello World!"
name = "Smartninja!"

print greeting

print greeting + " - AI "  # concat

print "*" * 20

print "Greeting: %s, Name: %s"%(greeting, name)
print "Greeting: {}, Name: {}".format(greeting, name)

print greeting == name
print greeting == "Hello World!"

print "3" + "3"
print int("3") + int("3")

# print int("3") + "3"      # TypeError
# print int("a")            # ValueError

