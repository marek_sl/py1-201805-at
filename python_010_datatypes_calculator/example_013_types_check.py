

print type(1)                           # <type 'int'>
print type(1.)                          # <type 'float'>
print type(1L)                          # <type 'long'>
print type(1+3j)                        # <type 'complex'>
print type("hello")                     # <type 'str'>
print type([1,2,3])                     # <type 'list'>
print type((1,2,3))                     # <type 'tuple'>
print type({"name": "pat", "age": 12})  # <type 'dict'>


print isinstance(1, int)    # True
print isinstance(1, float)  # False
print isinstance(1, str)    # False
