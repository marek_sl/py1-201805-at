import urllib2
import BeautifulSoup as BS

if __name__ == '__main__':
    url = 'https://scrapebook22.appspot.com/'
    response = urllib2.urlopen(url).read()
    soup = BS.BeautifulSoup(response)

    rows = soup.tbody.findAll("tr")
    for row in rows:
        datas = row.findAll("td")
        print("*" * 20)
        print(datas[0].string)
