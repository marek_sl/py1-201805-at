import urllib2
import BeautifulSoup as BS
import re


if __name__ == '__main__':
    url = 'https://scrapebook22.appspot.com'
    response = urllib2.urlopen(url).read()
    soup = BS.BeautifulSoup(response)

    lines = []

    links = soup.table.findAll("a")
    for link in links:
        subpage_link = url + link["href"]
        subresponse = urllib2.urlopen(subpage_link).read()
        subsoup = BS.BeautifulSoup(subresponse)

        email = subsoup.findAll("span", attrs={"class": "email"})[0].string
        gender = subsoup.findAll("span", attrs={"data-gender": True})[0].string
        city = subsoup.findAll("span", attrs={"data-city": True})[0].string
        age_text = subsoup.ul.find("li", text=re.compile("(Age)"))
        age = re.findall(r"(\d+)", age_text)[0]

        name = subsoup.findAll("h1")[1].text

        print "name: {}, email: {}, gender: {}, city: {}, age: {}".format(name, email,gender,city, age)

        line = ",".join([email, city, gender, age, name])
        lines.append(line)

    content = "\n".join(lines)

    with open("names_scrape.csv", "w") as f:
        f.write("email, city, gender, age, name\n")
        f.write(content)

