#!/usr/bin/env python
# -*- coding: UTF-8 -*-

def addition(x,y):
    return x + y

def subtraction(x,y):
    return x - y

def multiplication(x,y):
    return x * y

def division(x,y):
    return x / y

# guard, protection from being executed on import
if __name__ == '__main__':
    result = addition(10,23)
    print result
    result = subtraction(10,8)
    print result
    result = multiplication(4,2)
    print result
    result = division(27,3)
    print result
