import hw_lottery_numbers_solutions
# check if your function can pass the tests

def check_lottey_numbers():
    numbers = hw_lottery_numbers_solutions.lottery_generator()
    assert len(numbers) == 6, "should return 6 numbers"
    assert len(list(set(numbers))) == 6, "should return unique numbers"

if __name__ == '__main__':
    check_lottey_numbers()
    print "Passed Tests"
