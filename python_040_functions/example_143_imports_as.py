import math_func_src as CALC


def check_addition():
    assert CALC.addition(10, 20) == 30
    assert CALC.addition(10, -10) == 0


def check_subtraction():
    assert CALC.subtraction(10, 20) == -10


if __name__ == '__main__':
    check_addition()
    check_subtraction()
    print "Passed all tests"
